﻿DROP DATABASE IF EXISTS letrasunoene;
CREATE DATABASE IF NOT EXISTS letrasunoene;
USE letrasunoene;
CREATE TABLE a (
a1 int,
  a2 varchar (30),
  PRIMARY KEY (a1)
);
CREATE TABLE b (
b1 int,
  b2 varchar (30),
  PRIMARY KEY (b1)
);
CREATE TABLE r (
r1 int,
  a1 int,
  b1 int,
  PRIMARY KEY (a1, b1),
  UNIQUE KEY (b1),
  CONSTRAINT a1a FOREIGN KEY (a1) REFERENCES a (a1),
  CONSTRAINT b1b FOREIGN KEY (b1) REFERENCES b (b1)
);
CREATE TABLE rp (
a1 int,
  b1 int,
  r2 varchar (30),
  PRIMARY KEY (a1,b1,r2),
  CONSTRAINT rpr FOREIGN KEY (a1, b1) REFERENCES r (a1, b1)
);